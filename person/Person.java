package person;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import account.Account;
import main.IDGenerator;

public class Person implements Observer, Serializable {
	private int    personID;
	private String name;
	
	

	public Person(String name, int id) {
		setName(name);
		setPersonID(id);
	}
	
	public Person(String name){
		this(name, IDGenerator.instance().getNext());
	}

	public int getPersonID() {
		return this.personID;
	}

	public void setPersonID(int id) {
		this.personID = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + personID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (personID != other.personID)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "[Person] personID=" + personID + ", name=" + name;
	}

	@Override
	public void update(Observable observable, Object arg) {
		// TODO Auto-generated method stub
		Account account = (Account) observable;

		System.out.println("NOTIFICATION: " + this + " by " + account + " sum: " + arg);
	}

}
