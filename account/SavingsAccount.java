package account;

import java.util.Observer;

import exception.AmountExceeded;
import main.IDGenerator;

public class SavingsAccount extends Account{
	private int availableWithdrawals = 2;
	private double withdrawalLimit;
	private double dailyWithdrawal = 0;
	
	public SavingsAccount(int ID, Observer o) {
		super(ID, o);
		setAvailableWithdrawals(2);
		setWithdrawalLimit(1000);
		setDailyWithdrawal(0);
	}
	
	public SavingsAccount(Observer o) {
		this(IDGenerator.instance().getNext(), o);
	}

	@Override
	public void deposit(double amount) {
		setBalance(getBalance() + amount);
		setChanged();
		notifyObservers(amount);

	}

	@Override
	public void withdraw(double sum) throws AmountExceeded {
		if (getAvailableWithdrawals()+1 > 0) {
			if (getDailyWithdrawal() + sum <= getWithdrawalLimit()) {
				if (getBalance() >= sum) {

					setBalance(getBalance() - sum);

					setDailyWithdrawal(getDailyWithdrawal() + sum);
					decreaseAvailableWithdrawals();

					setChanged();
					notifyObservers(sum);

				} else {
					throw (new AmountExceeded());
				}

			} else {
				throw (new AmountExceeded());
			}

		} else {
			throw (new AmountExceeded());
		}

	}

	@Override
	public String toString() {
		return "SavingAccount [accountId=" + getAccountID() + ", balane=" + getBalance() + "]";
	}

	public int getAvailableWithdrawals() {
		return availableWithdrawals;
	}

	public void setAvailableWithdrawals(int availableWithdrawals) {
		this.availableWithdrawals = availableWithdrawals;
	}

	public void decreaseAvailableWithdrawals() {
		this.availableWithdrawals = getAvailableWithdrawals() - 1;
	}

	public double getWithdrawalLimit() {
		return withdrawalLimit;
	}

	public void setWithdrawalLimit(double withdrawalLimit) {
		this.withdrawalLimit = withdrawalLimit;
	}

	public double getDailyWithdrawal() {
		return dailyWithdrawal;
	}

	public void setDailyWithdrawal(double d) {
		this.dailyWithdrawal = d;
	}
}
