package account;

import java.util.Observer;

import exception.AmountExceeded;
import main.IDGenerator;

public class SpendingAccount extends Account{

	public SpendingAccount(int ID, Observer o){
		super(ID, o);
	}
	
	public SpendingAccount(Observer o){
		this(IDGenerator.instance().getNext(), o);
	}
	
	@Override
	public void deposit(double amount) {
		setBalance(getBalance() + amount);
		setChanged();
		notifyObservers(amount);		
	}

	@Override
	public void withdraw(double amount) throws AmountExceeded {
		if (getBalance() >= amount) {
			setBalance(getBalance() - amount);
			setChanged();
			notifyObservers(amount);
		} else {
			throw (new AmountExceeded());
		}		
	}
	
	@Override
	public String toString() {
		return "[SpendingAccount] AccountID=" + getAccountID() + ", balane=" + getBalance() + "]";
	}
}
