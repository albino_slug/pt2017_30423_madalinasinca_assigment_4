package account;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import exception.AmountExceeded;
import main.IDGenerator;

public abstract class Account extends Observable implements Serializable{
	private int    accountID;
	private double balance;
	
	public Account(int ID, Observer o){
		super.addObserver(o);
		setAccountID(ID);
		setBalance(0);
	}
	
	public void addObserver(Observer o){
		super.addObserver(o);
	}
	
	
	public abstract void deposit(double amount);
	public abstract void withdraw(double amount) throws AmountExceeded;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accountID;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass().getSuperclass() != obj.getClass().getSuperclass()) {

			System.out.println(getClass() + " " + obj.getClass());
			return false;
		}

		Account other = (Account) obj;
		if (accountID != other.accountID)
			return false;
		return true;
	}
	
	void setAccountID(int ID){
			this.accountID = ID;
	}
	
	void setBalance(double amount){
		this.balance = amount;
	}

	public int getAccountID() {
		return accountID;
	}

	public double getBalance() {
		return balance;
	}
	
	@Override
	public String toString() {
		return "[Account] AccountID=" + accountID + ", balance=" + balance + "]";
	}
}
