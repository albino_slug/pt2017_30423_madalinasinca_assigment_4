package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import account.Account;
import bank.Bank;
import person.Person;

public class PersonPanel extends JPanel{
	JScrollPane pane;
	JButton		addPerson;
	String[][] rowsData;
	Bank bank;
	int selectedRow;
	List<Person> persons;
	public PersonPanel(Bank bank){
		super();
		persons = bank.getPersons();
		this.bank = bank;
		pane = new JScrollPane();
		addPerson = new JButton("Add new person");
	}
	
	public void addPersonAL(ActionListener al){
		addPerson.addActionListener(al);
	}
	
	public void load(){
		this.setLayout(null);
		pane.setBounds(0, this.getHeight() / 10, this.getWidth(), this.getHeight());
		addPerson.setBounds(0, 0, this.getWidth(), this.getHeight() / 10);
		update();
		this.add(pane);
		this.add(addPerson);
		this.setVisible(true);
	}
	
	public Person getSelectedPerson(){
		return bank.getPerson(Integer.parseInt(rowsData[selectedRow][0]));
	}
	
	public void update(){
		persons = bank.getPersons();
		String[] columnNames = new String[]{"id", "name"};
		rowsData = new String[persons.size()][2];
		
		int i = 0;
		for (Person person :persons){
			rowsData[i][0] = "" + person.getPersonID();
			rowsData[i++][1] = person.getName();
			
		}
		final JTable table = new JTable(rowsData, columnNames);
		table.setBounds(0, 0, pane.getWidth(), pane.getHeight());
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
        table.addMouseListener(new MouseAdapter() {
        	  public void mouseClicked(MouseEvent e) {
        	      JTable target = (JTable)e.getSource();
        	      int row = target.getSelectedRow();
        	      selectedRow = row;
        	      int column = target.getSelectedColumn();
        	      UIAction action = askAction();
        	      if (action == UIAction.CHANGE_NAME){
        	    	  bank.getPerson(Integer.parseInt(rowsData[row][0])).setName(askName());
        	      }
        	      else if (action == UIAction.DELETE){
        	    	  bank.removePerson(Integer.parseInt(rowsData[row][0]));
        	      }
        	      update();
        	    }
        	});

        pane.setViewportView(table);
	}
	
	private UIAction askAction(){
		UIAction[] options = UIAction.values();
		int n = JOptionPane.showOptionDialog(null,
			    "What type of action whould you like to perform",
			    "",
			    JOptionPane.YES_NO_CANCEL_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    options,
			    options[0]);
		return options[n];
	}
	
	private String askName(){
		return JOptionPane.showInputDialog(null, "Name of thes person", null, JOptionPane.INFORMATION_MESSAGE, null, null, "").toString();
	}
	
}
