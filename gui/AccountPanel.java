package gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;
import java.util.function.Function;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import account.Account;
import account.SavingsAccount;
import account.SpendingAccount;
import bank.Bank;
import person.Person;

public class AccountPanel extends JPanel{
	JScrollPane pane;
	JButton		addAccount;
	Map<Account, Person> accounts;
	Bank bank;
	int selectedRow;
	public AccountPanel(Bank bank){
		super();
		this.bank = bank;
		this.accounts = bank.getAllAccounts();
		pane = new JScrollPane();
		addAccount = new JButton("create new account");
	}
	
	public void addAccountAL(ActionListener al){
		addAccount.addActionListener(al);
	}
	
	public void load(){
		this.setLayout(null);
		pane.setBounds(0, this.getHeight() / 10, this.getWidth(), this.getHeight());
		addAccount.setBounds(0, 0, this.getWidth(), this.getHeight() / 10);
		update();
		this.add(pane);
		this.add(addAccount);
		this.setVisible(true);
	}
	
	public void update(){
		this.accounts = bank.getAllAccounts();
		String[] columnNames = new String[]{"type", "id", "owner", "ownerId", "balance"};
		String[][] rowsData = new String[accounts.size()][5];
		
		int i = 0;
		for (Account account : accounts.keySet()){
			rowsData[i][0] = account.getClass().getSimpleName();
			rowsData[i][1] = "" + account.getAccountID();
			rowsData[i][2] = accounts.get(account).getName();
			rowsData[i][3] = "" + accounts.get(account).getPersonID();
			rowsData[i++][4] = "" + account.getBalance();
			
		}
		final JTable table = new JTable(rowsData, columnNames);
		table.setBounds(0, 0, pane.getWidth(), pane.getHeight());
		table.setPreferredScrollableViewportSize(new Dimension(500, 70));
        table.setFillsViewportHeight(true);
        table.addMouseListener(new MouseAdapter() {
        	  public void mouseClicked(MouseEvent e) {
        	      JTable target = (JTable)e.getSource();
        	      int row = target.getSelectedRow();
        	      selectedRow = row;
        	      int column = target.getSelectedColumn();
        	      int sum = 0;
        	      UIAction action = askAction();
        	      if (action == UIAction.DEPOSIT || action == UIAction.WITHDRAW){
        	    	  sum = askSum();
        	    	  if (action == UIAction.DEPOSIT){
        	    		  bank.depositMoney(Integer.parseInt(rowsData[row][3]), Integer.parseInt(rowsData[row][1]), sum);
        	    	  }
        	    	  else{
        	    		  bank.withdrawMoney(Integer.parseInt(rowsData[row][3]), Integer.parseInt(rowsData[row][1]), sum);
        	    	  }
        	      }
        	      else if (action == UIAction.DELETE){
        	    	  bank.closeAccount(Integer.parseInt(rowsData[row][3]), Integer.parseInt(rowsData[row][1]));
        	      }
        	      update();
        	    }
        	});

        pane.setViewportView(table);
	}
	
	private UIAction askAction(){
		UIAction[] options = UIAction.values();
		int n = JOptionPane.showOptionDialog(null,
			    "What type of action whould you like to perform",
			    "",
			    JOptionPane.YES_NO_CANCEL_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    options,
			    options[0]);
		return options[n];
	}
	
	private int askSum(){
		return Integer.parseInt(JOptionPane.showInputDialog(null, "Deposit / withdraw from selected account", null, JOptionPane.INFORMATION_MESSAGE, null, null, "").toString());
	}
	
	private int askAccountType(){
		Integer[] options = {0, 1};
		int n = JOptionPane.showOptionDialog(null,
			    "What type of account whould you like to open?",
			    "1 for Saving, 0 for Spending",
			    JOptionPane.YES_NO_CANCEL_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    options,
			    options[0]);
		return options[n];
	}
	
}
