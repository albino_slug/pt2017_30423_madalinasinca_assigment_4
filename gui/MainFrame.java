package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import account.Account;
import account.SavingsAccount;
import account.SpendingAccount;
import bank.Bank;
import person.Person;

public class MainFrame extends JFrame{
	private AccountPanel accountPanel;
	private PersonPanel personPanel;
	private Bank bank;
	public MainFrame(Bank bank){
		super();
		this.bank = bank;
		accountPanel = new AccountPanel(bank);
		personPanel = new PersonPanel(bank);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		accountPanel.addAccountAL(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				bank.openAccountForPerson(personPanel.getSelectedPerson(), askAccount(personPanel.getSelectedPerson()));
				personPanel.update();
				accountPanel.update();
			}
		});
		personPanel.addPersonAL(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Person p = new Person(askName());
				bank.openAccountForPerson(p, askAccount(p));
				personPanel.update();
				accountPanel.update();
			}
			
		});
	}
	
	private String askName(){
		return JOptionPane.showInputDialog(null, "Name of thes person", null, JOptionPane.INFORMATION_MESSAGE, null, null, "").toString();
	}
	
	public void load(){
		this.setLayout(null);
		this.setBounds(0, 0, 800, 600);
		accountPanel.setBounds(0, 0, this.getWidth() / 2, this.getHeight());
		personPanel.setBounds(this.getWidth() / 2, 0, this.getWidth() / 2, this.getHeight());
		accountPanel.load();
		personPanel.load();
		this.add(personPanel);
		this.add(accountPanel);
		this.setVisible(true);
	}
	
	private Account askAccount(Person p){
		Account acc;

		if (askAccountType() == 1){//saving
			acc = new SavingsAccount(p);
		}else{//spending
			acc = new SpendingAccount(p);
		}
		return (acc);
		
	}
	
	private int askAccountType(){
		Integer[] options = {0, 1};
		int n = JOptionPane.showOptionDialog(null,
			    "What type of account whould you like to open?",
			    "1 for Saving, 0 for Spending",
			    JOptionPane.YES_NO_CANCEL_OPTION,
			    JOptionPane.QUESTION_MESSAGE,
			    null,
			    options,
			    options[0]);
		return options[n];
	}
//	addAccount.addActionListener(new ActionListener(){
//		@Override
//		public void actionPerformed(ActionEvent arg0) {
//			System.out.println("[AccountPanel] create new account");
//			Person p = bank.getPerson(Integer.parseInt(rowsData[selectedRow][3]));
//			Account acc;
//			if (askAccountType() == 1){//saving
//				acc = new SavingsAccount(3, p);
//			}else{//spending
//				acc = new SpendingAccount(3, p);
//			}
//			bank.openAccountForPerson(p, acc);
//			update();
//		}
//	});
}
