package tests;

import org.junit.Test;

import account.Account;
import account.SavingsAccount;
import bank.Bank;
import person.Person;

public class TestSuit {
	
	@Test
	public void test1(){
		Bank b = new Bank();
		Person p = new Person("andrei");
		Account a = new SavingsAccount(p);
		b.openAccountForPerson(p, a);
		b.removePerson(p.getPersonID());
		assert(b.getAllAccounts().size() == 0);
	}
}
