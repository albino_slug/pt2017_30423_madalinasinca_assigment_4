package main;

public class IDGenerator {
	private static IDGenerator instance;
	private int val;
	
	public int getVal() {
		return val;
	}
	
	public void setVal(int val) {
		this.val = val;
	}
	
	public static IDGenerator instance(){
		if (instance == null){
			instance = new IDGenerator();
		}
		return (instance);
	}
	
	public int getNext(){
		return (val++);
	}
}
