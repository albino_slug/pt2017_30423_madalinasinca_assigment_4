package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import account.Account;
import account.SavingsAccount;
import bank.Bank;
import gui.MainFrame;
import person.Person;

public class Main {
	static Bank bank = new Bank();
	public static void main(String[] args) {
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
	        public void run() {
	        	bank.setLastId(IDGenerator.instance().getVal());
	           saveToFile(bank);
	        }
	    }, "Shutdown-thread"));
		
		bank = loadFromFile();
		IDGenerator.instance().setVal(bank.getLastId());
		MainFrame frame = new MainFrame(bank);
		frame.load();

		System.out.println(bank.toString());
	}
	
	private static void saveToFile(Bank bank){
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("bank.ser"));
			oos.writeObject(bank);
			oos.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Bank loadFromFile(){
		Bank b = null;
		// read object from file
		try{
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream("bank.ser"));
			b = (Bank) ois.readObject();
			b.updateObservers();
			ois.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return (b);
	}
	
//	Person p1 = new Person("Anisoara", 23);
//	Person p2 = new Person("Gelu", 12);
//	Person p3 = new Person("Onisifor", 43);
//	Account a1 = new SavingsAccount(24, p1);
//	Account a2 = new SavingsAccount(25, p1);
//	Account a3 = new SavingsAccount(26, p2);
//	Account a4 = new SavingsAccount(27, p2);
//	Account a5 = new SavingsAccount(28, p3);
//	bank.openAccountForPerson(p1, a1);
//	bank.openAccountForPerson(p1, a2);
//	bank.openAccountForPerson(p2, a3);
//	bank.openAccountForPerson(p2, a4);
//	bank.openAccountForPerson(p3, a5);

}
