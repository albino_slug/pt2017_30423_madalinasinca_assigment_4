package bank;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import account.Account;
import account.SavingsAccount;
import exception.AmountExceeded;
import person.Person;

public class Bank implements BankProcedures, Serializable{
	private Map<Person, HashSet<Account>> accountMap;
	private int lastId;
	
	public Bank() {
		accountMap = new HashMap<Person, HashSet<Account>>();
	}

	public void updateObservers(){
		for (Entry<Person, HashSet<Account>> entry : accountMap.entrySet()){
			for (Account account : entry.getValue()){
				account.addObserver(entry.getKey());
			}
		}
	}
	
	public void removePerson(int id){
		accountMap.remove(getPerson(id));
	}
	
	public Person getPerson(int id){
		for (Person person : accountMap.keySet()){
			if (person.getPersonID() == id){
				return person;
			}
		}
		return (null);
	}
	
	@Override
	public void openAccountForPerson(Person person, Account account) {
		assert(allIsWell());
		// if the person already exists
		if (accountMap.containsKey(person)) {
			accountMap.get(person).add(account);
			System.out.println("[BANK | OpenAccount] Added new account to an existing person;");
			System.out.println(account + " " + person);
			return;
		}
		// person already exists in bank DB
		else{
			HashSet<Account> auxiliaryHashSet = new HashSet<Account>();
			auxiliaryHashSet.add(account);
			accountMap.put(person, auxiliaryHashSet);
			System.out.println("[BANK | OpenAccount] Added new account to a new person;");
			System.out.println(account + " " + person);
			account.addObserver(person);
		}	
		assert(allIsWell());
	}

	@Override
	public void depositMoney(int personID, int accountID, double amount) {
		assert(allIsWell());
		// Create person and account object:
		Person  auxiliaryPerson = new Person("", personID);
		Account auxiliaryAccount = new SavingsAccount(accountID, auxiliaryPerson);
		
		// Find the accounts of the created person (if any)
		HashSet<Account> auxiliaryAccountSet = accountMap.get(auxiliaryPerson);
		
		if (accountMap.containsKey(auxiliaryPerson)) {

			// Search for the given account:
			for (Account currentAccount : auxiliaryAccountSet) {
				if (currentAccount.equals(auxiliaryAccount)) {
					// The account was found:
					currentAccount.deposit(amount);
					return;
				}
			}
			System.out.println("[BANK | Deposit Money] The specified person - account pair was not found;");
		} else {
			System.out.println("[BANK | Deposit Money] The specified person was not found!");
		}
		assert(allIsWell());
	}

	@Override
	public void withdrawMoney(int personId, int accountId, double sum) {
		assert(allIsWell());
		assert sum > 0 : "Sum has to be greater than 0!";

		// Create person and account object:
		Person tempPerson = new Person("", personId);
		Account tempAccount = new SavingsAccount(accountId, tempPerson);

		// Find the accounts of the created person:
		HashSet<Account> tempAccountSet = accountMap.get(tempPerson);

		// Search for the given account:
		for (Account currAccount : tempAccountSet) {
			if (currAccount.equals(tempAccount)) {

				// The account was found:
				try {
					// Try to withdraw money:
					currAccount.withdraw(sum);
				} catch (AmountExceeded e) {
					System.out.println("Transaction failed!");
				}
				return;
			}
		}
		System.out.println("The specified person-account pair was not found!");
		assert(allIsWell());
	}

	@Override
	public void closeAccount(int personID, int accountID) {
		assert(allIsWell());
		// Create person and account object:
		Person tempPerson = new Person("", personID);
		Account tempAccount = new SavingsAccount(accountID, tempPerson);

		if (accountMap.containsKey(tempPerson)) {
			// Find the accounts of the created person:
			HashSet<Account> tempAccountSet = accountMap.get(tempPerson);

			// Search for the given account:
			for (Account currAccount : tempAccountSet) {
				if (currAccount.equals(tempAccount)) {

					// The account was found - delete it:
					tempAccountSet.remove(currAccount);
					return;
				}
			}
			System.out.println("Account not found!");
			return;
		} else {
			System.out.println("Person not found!");
		}
		assert(allIsWell());
	}
	
	/**
	 * Returns each pair of person-account as a Map. 
	 * A person can appear multiple times, while an account
	 * only once
	 */
	public Map<Account, Person> getAllAccounts() {
		assert(allIsWell());
		Map<Account, Person> persAccPairs = new HashMap<Account, Person>();
		
		for (Map.Entry<Person, HashSet<Account>> entry : accountMap.entrySet())
		{
		    Iterator<Account> i = entry.getValue().iterator();
		    
		    while(i.hasNext()){
		    	persAccPairs.put(i.next(), entry.getKey());
		    	//System.out.println("[TABLE]: " + entry.getKey() + persAccPairs.get(entry.getKey()));
		    	
		    }
		}
		assert(allIsWell());
		return persAccPairs;

	}

	@Override
	public boolean allIsWell() {

		Iterator<Entry<Person, HashSet<Account>>>  i = accountMap.entrySet().iterator();
		
		while (i.hasNext()){
			Entry<Person, HashSet<Account>> entry = i.next();
			if (entry.getKey() == null)
				return false;
			if (entry.getValue() == null)
				return false;
			if (entry.getValue().isEmpty())
				return false;
			
		}
		
		return true;
	}

	public List<Person> getPersons(){
		return (accountMap.keySet().stream().collect(Collectors.toList()));
	}
	
	@Override
	public String toString() {
		return "Bank [accountMap=" + accountMap + "]";
	}

	public int getLastId() {
		return lastId;
	}

	public void setLastId(int lastId) {
		this.lastId = lastId;
	}

}
 