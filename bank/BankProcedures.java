package bank;

import account.Account;
import person.Person;

public interface BankProcedures {

	/**
	 * The fields of all the parameters should be initialized. Search for an
	 * already existing Person and adds the account to it. Otherwise stores the
	 * new Person.
	 * 
	 * @pre   allIsWell(); person.personId != null; account != null && should
	 *        not already exist
	 * @post  allIsWell(); number of persons same or greater with one then
	 *        before; number of accounts greater than before
	 * 
	 * @param person
	 *            the Person to be added
	 * @param account
	 *            the Account to be added
	 */
	void openAccountForPerson(Person person, Account account);

	
	/**
	 * @pre   allIsWell()
	 * @post  allIsWell()
	 * 
	 * @param accountId
	 *            destination account ID
	 * @param amount
	 *            sum to be added to account
	 */
	void depositMoney(int personID, int accountID, double amount);

	
	/**
	 * @pre   allIsWell()
	 * @post  allIsWell()
	 * 
	 * @param accountID
	 *            the ID of the account from which to remove money
	 * @param sum
	 *            the sum to be removed from the account
	 */
	void withdrawMoney(int personID, int accountID, double amount);

	
	/**
	 * Warning! If this is the last account of a person, the bank will remain in
	 * a inconsistent state. Please also remove the person in this case or add a
	 * new account.
	 * 
	 * @pre   allIsWell();
	 * @post  allIsWell(); if !allIsWell() then remove the person that lost
	 *        its account or throw exception;
	 * 
	 * @param accountID
	 *            the ID of the account to be removed
	 */
	void closeAccount(int personID, int accountID);

	
	/**
	 * Check if each there are null elements and if each person has at least one
	 * account.
	 * 
	 * @return true if the Bank is in a good state and false if the data in the
	 *         bank is corrupted or damaged
	 */
	boolean allIsWell();

}
